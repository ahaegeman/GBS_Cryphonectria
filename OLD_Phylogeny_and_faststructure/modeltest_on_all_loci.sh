#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to run "jmodeltest2" on a number of fasta files containing alignments
# The fasta files should be located in 1 folder, as specified below.
# The script will make a new folder with modeltest files with the results
# it will then process the results and report the number of times each model was selected as best model

#Specify here the folder where your samples are:
INPUT_FOLDER=/home/genomics/ahaegeman/GBS_Phytophthora/R_tests_op_server/loci_shared_by_all/fasta_files

date
echo

#change directory to specified folder
cd $INPUT_FOLDER

##################
#MAKE PHYLIP FILES
##################
# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
#FILES=( *.fasta )
#Loop over all files and make phylip files to use as input for model test
#for f in "${FILES[@]}" 
#do 
#Define the variable SAMPLE who contains the basename where the extension is removed
#SAMPLE=`basename $f .fasta`
#echo
#echo "Making phylip file from fasta file $SAMPLE ..."
#MAKE PHYLIP FILE
#java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d "$SAMPLE".fasta -getPhylip
#echo "Done making phylip file from fasta file $SAMPLE."
#done

###############
#RUN MODELTESTS
###############

#mkdir modeltests

#now loop over all fasta files
FILES=( *.fasta )
#Loop over all files and do model test
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f .fasta`
echo
echo "Running modeltest on fasta file $SAMPLE ..."
#Run modeltest
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d "$SAMPLE".fasta -g 4 -BIC -i -f -s 11 -t BIONJ -S NNI -o ./modeltests/"$SAMPLE"_modeltest.txt
echo "Done running modeltest on fasta file $SAMPLE."
done

###################
#ANALYZE MODELTESTS
###################

cd modeltests

#now loop over all _modeltest.txt files
FILES=( *_modeltest.txt )
#Loop over all files and do model test
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f _modeltest.txt`
#extract best model from file
MODEL=`tail -n 1 "$SAMPLE"_modeltest.txt | cut -f 2`

#write best model to new file
echo -e "$SAMPLE\t$MODEL" >> bestmodels.txt	#-e = enables interpretation of backslash characters

done

#count number of times each model occurs and report the top ones
echo "The number of times each model occurs:"
cut -f2 bestmodels.txt | sort | uniq -c | sort -nr

echo
date

