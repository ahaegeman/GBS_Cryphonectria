#!/usr/bin/perl

$|=1;
use strict;
use warnings;
use Bio::AlignIO;
use Bio::SeqIO;

#############
#input files#
#############
my $datafile= $ARGV[0];
my $dir = "fasta_files";
system("mkdir $dir");
die "usage perl convert_genotypes_to_alignments_per_locus.pl genotypes.txt" if (scalar @ARGV != 1);


#####################
#initiate variabeles#
#####################
my ($lijn,@line,@names,$locus,$length,@elementlist);
my $i=0;
my $j="-999";
my $k=0;
my $test;


########
#SCRIPT#
########

#read genotypes file and loop over all lines (=loci)
open (IP,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP>)		
	{	chomp $lijn;
		if ($. == 1){		#store names of isolates in first line
			@names = split (/\t/,$lijn);
			shift(@names); #remove the first element (which is "poplocID")
		}
		else{	#from second line on, make a file per locus containing the sequences from all samples
			
			#prepare data
			@line = split (/\t/,$lijn);
			$locus = $line[0];	#first element of line contains the locus number, to be used as file name
			open(OP,">$dir/".$locus.".fasta");	#initiate file with locusname
			shift(@line); #remove the first element (which is the locus name)
			
			#determine the length of the locus by looping over all elements of a line, and when the element is different than -999, save the length
			foreach my $element (@line){
				if ($element ne "-999") {
					$length=length($element);
					}
				}			
						
			#loop over the elements (=sequences) in the line and print to output
			foreach my $element (@line){
				if ($element ne "-999") {
					@elementlist = split (/\//,$element);
					if (scalar(@elementlist) == 1) {	#if only 1 allele
						print OP ">$names[$i]\n";
						print OP "$elementlist[0]\n";
						}
					else {	#if more than 1 allele
						#open TEMP file to store sequence data of the alleles in fasta format
						open (TEMP,">temp.fasta");
						foreach my $allele (@elementlist){
							print TEMP ">$k\n";
							print TEMP "$allele\n";
							$k++;
						}
						close(TEMP);
						
						#now calculate consensus sequence from alignment
						my $consensus = lc(consensus("temp.fasta"));
                          							
						#write consensus sequence to output
						print OP ">$names[$i]\n";
						print OP "$consensus\n";
						
						#initiate variables, make TEMP file empty
						$k=0;
						system("rm temp.fasta");
					}
				}
				else {	#if -999, print "-" for the length of the locus
					#print nnnnn as locus
					#print OP ">$names[$i]\n";
					#print OP "n" x $length;
					#print OP "\n";
				}
				$i++;	#counter variable
			}
			
			$i=0;	#after finishing processing the line, but the counter back at zero
		}
	}
	
#function "consensus" with as argument a fasta file with aligned sequences, the function returns the consensus sequence with iupac code	
sub consensus {
	my $seqname = "consensus";
	my $in  = Bio::AlignIO->new(-file   => @_ , 
                          -format => 'fasta'); 
	my $ali = $in->next_aln(); 
	my $iupac_consensus = $ali->consensus_iupac();   # dna/rna alignments only 
	my $seq = Bio::Seq->new(-seq => "$iupac_consensus",   
                        -display_id => $seqname); 
	my $seq2 = $seq->seq;
	return $seq2;
}