#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to run "raxml" on a number of fasta files containing alignments
# The fasta files should be located in 1 folder, as specified below.
# The script will make a new folder with phylip files, it will then run raxml on all these phylip files.

#Specify here the folder where your samples are:
INPUT_FOLDER=/home/genomics/ahaegeman/GBS_Phytophthora/R_tests_op_server/loci_shared_by_all/fasta_files_shared_loci

COUNTS=0
LOCI=0

date
echo

#change directory to specified folder
cd $INPUT_FOLDER

#LOOP OVER SAMPLES
# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *_SNPs.phy )
#Loop over all files and make phylip files to use as input for model test
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f _SNPs.phy`
LOCI=$((LOCI+1))
SITES=`head -n 1 "$SAMPLE"_SNPs.phy| cut -d ' ' -f2`
COUNTS=$((COUNTS+SITES))

done

echo "There are $COUNTS variable sites in $LOCI loci."
echo

