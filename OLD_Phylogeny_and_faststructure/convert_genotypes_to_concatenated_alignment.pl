#!/usr/bin/perl

$|=1;
use strict;
#use warnings;
use Bio::AlignIO;
use Bio::SeqIO;

#############
#input files#
#############
my $datafile= $ARGV[0];
my $outputfile=$ARGV[1];
die "usage perl convert_genotypes_to_concatenated_alignment.pl genotypes.txt output.fa" if (scalar @ARGV != 2);

#####################
#initiate variabeles#
#####################
my ($lijn,@line,@names,$length,@elementlist,%seqs,$empty,$id);
my $i=0;
my $j="-999";
my $k=0;
open(OP,">$outputfile");

########
#SCRIPT#
########

#read genotypes file and loop over all lines (=loci)
open (IP,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP>)		
	{	chomp $lijn;
		if ($. == 1){		#store names of isolates in first line
			@names = split (/\t/,$lijn);
			shift(@names); #remove the first element (which is "poplocID")
		}
		else{	#from second line on, make a file per locus containing the sequences from all samples
			
			#prepare data
			@line = split (/\t/,$lijn);
			shift(@line); #remove the first element (which is the locus name)
			
			#determine the length of the locus by looping over all elements of a line, and when the element is different than -999, save the length
			foreach my $element (@line){
				if ($element eq "consensus") {
					$length=0;	#if consensus sequence, the length of the SNPs is zero
					}
				elsif ($element ne "-999" and $element ne "consensus") {
					@elementlist = split (/\//,$element);
					$length=length($elementlist[0]);	#calculate length of the first allele
					}
				}			
						
			#loop over the elements (=sequences) in the line and add to hash with as ID the sample name and as value the previous value with the new value concatenated
			foreach my $element (@line){
				if ($element eq "consensus"){
					
				}
				elsif ($element ne "-999") {
					@elementlist = split (/\//,$element);
					if (scalar(@elementlist) == 1) {	#if only 1 allele
						$seqs{$names[$i]}=$seqs{$names[$i]}.$elementlist[0];	#hash with as key the name of the sample and as value the concatenated sequence
						}
					else {	#if more than 1 allele
						#open TEMP file to store sequence data of the alleles in fasta format
						open (TEMP,">temp.fasta");
						foreach my $allele (@elementlist){
							print TEMP ">$k\n";
							print TEMP "$allele\n";
							$k++;
						}
						close(TEMP);
						
						#now calculate consensus sequence from alignment
						my $consensus = lc(consensus("temp.fasta"));
                          							
						#write consensus sequence to hash
						$seqs{$names[$i]}=$seqs{$names[$i]}.$consensus;
						
						#initiate variables, make TEMP file empty
						$k=0;
						system("rm temp.fasta");
					}
				}
				else {	#if -999, add "-" for the length of the locus
					#print OP ">$names[$i]\n";
					$empty="-" x $length;
					#print "$empty\n";
					$seqs{$names[$i]}=$seqs{$names[$i]}.$empty;
				}
				$i++;	#counter variable
			}
			
			$i=0;	#after finishing processing the line, but the counter back at zero
		}
	}
	
#when finishing reading the data, print the resulting hash %seqs to a fasta formatted file
foreach $id (keys %seqs) {
    print OP ">$id\n";
    print OP "$seqs{$id}\n";
}
	
#function "consensus" with as argument a fasta file with aligned sequences, the function returns the consensus sequence with iupac code	
sub consensus {
	my $seqname = "consensus";
	my $in  = Bio::AlignIO->new(-file   => @_ , 
                          -format => 'fasta'); 
	my $ali = $in->next_aln(); 
	my $iupac_consensus = $ali->consensus_iupac();   # dna/rna alignments only 
	my $seq = Bio::Seq->new(-seq => "$iupac_consensus",   
                        -display_id => $seqname); 
	my $seq2 = $seq->seq;
	return $seq2;
}