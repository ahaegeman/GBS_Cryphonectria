#!/usr/bin/perl

$|=1;
use strict;
use warnings;
use Bio::AlignIO;
use Bio::SeqIO;

#############
#input files#
#############
my $datafile= $ARGV[0];
my $dir = "fasta_files";
system("mkdir $dir");
die "usage perl convert_genotypes_to_alignments_per_locus_all_alleles.pl genotypes.txt" if (scalar @ARGV != 1);


#####################
#initiate variabeles#
#####################
my ($lijn,@line,@names,$locus,$length,@elementlist);
my $i=0;
my $j="-999";
my $k=1;
open(OP2,">mappingfile.txt");


########
#SCRIPT#
########

#read genotypes file and loop over all lines (=loci)
open (IP,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP>)		
	{	chomp $lijn;
		if ($. == 1){		#store names of isolates in first line
			@names = split (/\t/,$lijn);
			shift(@names); #remove the first element (which is "poplocID")
		}
		else{	#from second line on, make a file per locus containing the sequences from all samples
			
			#prepare data
			@line = split (/\t/,$lijn);
			$locus = $line[0];	#first element of line contains the locus number, to be used as file name
			open(OP,">$dir/".$locus.".fasta");	#initiate file with locusname
			shift(@line); #remove the first element (which is the locus name)
			
			#determine the length of the locus by looping over all elements of a line, and when the element is different than -999, save the length
			foreach my $element (@line){
				if ($element ne "-999") {
					$length=length($element);
					}
				}			
						
			#loop over the elements (=sequences) in the line and print to output
			foreach my $element (@line){
				if ($element ne "-999") {
					@elementlist = split (/\//,$element);
					if (scalar(@elementlist) == 1) {	#if only 1 allele
						print OP ">".$locus."_1_".$names[$i]."\n";
						print OP "$elementlist[0]\n";
						print OP2 $locus."_1_".$names[$i]."\t".$names[$i]."\n";
						}
					else {	#if more than 1 allele
						foreach my $allele (@elementlist){
							print OP ">".$locus."_".$k."_".$names[$i]."\n";
							print OP "$allele\n";
							print OP2 $locus."_".$k."_".$names[$i]."\t".$names[$i]."\n";
							$k++;
						}			
						#initiate variables
						$k=1;
					}
				}
				else {	#if -999, print "-" for the length of the locus
					#print nnnnn as locus
					#print OP ">$names[$i]\n";
					#print OP "n" x $length;
					#print OP "\n";
				}
				$i++;	#counter variable
			}
			
			$i=0;	#after finishing processing the line, but the counter back at zero
		}
	}