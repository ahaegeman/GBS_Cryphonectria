#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script is used to calculate a phylogenetic tree from the output of Gibpss.
# It consists of the following steps:
# Step 1: the genotypes file is converted to fasta files (1 file per locus), each allele is mentioned separately in the fasta file.
# Step 2: new alignments (in phylip format) are made where the invariate SNPs are removed for each locus
# Step 3: A phylogenetic tree is made per locus, including 100 bootstraps
# Step 4: The resulting bootstraps from the trees are resampled to multilocus bootstraps
# Step 5: The multilocus bootstraps are used to make coalescent consensus trees using NJstm
# Step 6: A majority rule consensus tree is made from the resulting NJstm trees.
#####################################################

###################
#PARAMETER SETTINGS
###################
#Specify here the folder where all scripts needed for this analysis are:
SCRIPTS_FOLDER=/home/genomics/ahaegeman/GBS_Phytophthora/R_tests_op_server/loci_shared_by_all/scripts
#Specify here the folder where your genotypes file is:
INPUT_FOLDER=/home/genomics/ahaegeman/GBS_Phytophthora/R_tests_op_server/loci_shared_by_all/clade1_shared70
#Specify here the name of your genotypes file:
GENOTYPES_FILE=genotypes_sharedloci70.txt
#Specify the number of threads you want to use while running RAXML
THREADS=12
#specify the number of bootstraps you want to do for each locus tree
BSLOCUS=150
#specify the number of bootstraps you want to make from multilocusbootstrapping (!! note that since some genes will be by chance selected more often than others, we need that the number of multilocusbootstraps < the number of locusbootstraps. For example, with locusbootstraps = 200, usually multilocusbootstraps can be at most around 160.
BSMULTI=100

date
echo

######################################################
#STEP1: convert genotypes file to separate fasta files
######################################################
echo "Step 1: converting genotypes file to fasta files per locus..."

#change directory to folder where the data is
cd $INPUT_FOLDER
mkdir trees #make a directory "trees". A directory called "fasta_files" is also created by the perl script to convert the genotypes file to fasta files.
#Run perl script to create fasta files for each locus. These are stored in a newly created folder called "fasta_files".
#The script also creates a mappingfile.txt, this file maps the names of the alleles to the isolate names and will be used for NJstm.
perl $SCRIPTS_FOLDER/convert_genotypes_to_alignments_per_locus_all_alleles.pl $GENOTYPES_FILE

echo
echo "Done with step 1: converting genotypes file to fasta files per locus."
echo
##############################
#STEP2: remove invariante SNPs
##############################
echo "Step 2: removing invariant SNPs in all fasta files..."

#STEP 2.1: MAKE PHYLIP FILES
# Define the variable FILES that contains all the fasta files. 
FILES=($INPUT_FOLDER/fasta_files/*.fasta)
#Loop over all files and make phylip files to use as input for the script "extractVariantSites.py"
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f .fasta`
echo
echo "Making phylip file from fasta file $SAMPLE ..."
#MAKE PHYLIP FILE
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta -getPhylip
echo "Done making phylip file from fasta file $SAMPLE."
done

#STEP 2.2: USE extractVariantSites.py SCRIPT TO REMOVE INVARIANT SITES FROM PHYLIP FILES, see https://github.com/btmartin721/extractVariantSites4raxml
#now loop over all phylip files
FILES=($INPUT_FOLDER/fasta_files/*.fasta.phy)
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f .fasta.phy`
echo
echo "Remove invariant files for $SAMPLE ..."
python3 $SCRIPTS_FOLDER/extractVariantSites.py -p $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta.phy -l -o $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs

echo "Done removing invariant files for $SAMPLE."
done
echo
echo "Done with step 2: removing invariant SNPs in all fasta files."
echo
##############################
#STEP3: Make phylogenetic trees per locus, including 100 bootstraps
##############################
echo "Step 3: Making trees per locus..."
FILES=($INPUT_FOLDER/fasta_files/*_SNPs.phy)
for f in "${FILES[@]}" 
do 
SAMPLE=`basename $f _SNPs.phy`
echo "Running RAXML on $SAMPLE ..."
raxml -m ASC_GTRGAMMA --asc-corr=lewis -s $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs.phy -p 12345 -x 12345 -n $SAMPLE -T $THREADS -w $INPUT_FOLDER/trees -f a -# $BSLOCUS >> raxml_log.txt
echo "Done running RAXML on $SAMPLE."
echo
done
echo
echo "Done with step 3: making trees per locus."
echo
##############################
#STEP4: Make multilocus bootstrapped trees
##############################
echo "Step 4: Making multilocus bootstrapped trees..."
#STEP 4.1: First make the folder structure necessary to make multilocus bootstraps. There should be subfolders for all genes, and these should contain 1 file with the bootstrapped trees in there.
mkdir $INPUT_FOLDER/trees/bootstraps
FILES=($INPUT_FOLDER/trees/RAxML_bootstrap.*)
for f in "${FILES[@]}" 
do 
LOCUS=${f#$INPUT_FOLDER/trees/RAxML_bootstrap.}	#remove the prefix and store the locus number in the variable LOCUS
mkdir $INPUT_FOLDER/trees/bootstraps/$LOCUS	#make directory with locusname
cp $f $INPUT_FOLDER/trees/bootstraps/$LOCUS/RAxML_bootstrap	#copy the boostrap tree file to the locus directory and call it RAxML_bootstrap
done
#STEP 4.2 Now do multilocus bootstrapping as described here: https://github.com/smirarab/multi-locus-bootstrapping
#Example: multilocus_bootstrap_new.sh [number of replicates] [dir] [FILENAME] [outdir] [outname] [sampling] [weightfile] [random seed]
mkdir $INPUT_FOLDER/trees/multilocusbootstraps
$SCRIPTS_FOLDER/multilocus_bootstrap_new.sh $BSMULTI $INPUT_FOLDER/trees/bootstraps RAxML_bootstrap $INPUT_FOLDER/trees/multilocusbootstraps BS genesite - 12345
echo
echo "Done with step 4: Making multilocus bootstrapped trees."
echo
############################################
#STEP5: Perform NJstm on all multilocus bootstraps
############################################
echo "Step 5: Making consensus trees using NJstm..."
mkdir $INPUT_FOLDER/trees/NJstm
FILES=($INPUT_FOLDER/trees/multilocusbootstraps/BS.*)
for f in "${FILES[@]}" 
do 
SAMPLE=${f#$INPUT_FOLDER/trees/multilocusbootstraps/BS.}	#remove the prefix and store the locus number in the variable LOCUS
Rscript $SCRIPTS_FOLDER/njstm.r $INPUT_FOLDER/trees/multilocusbootstraps/BS.$SAMPLE mappingfile.txt reweighed $INPUT_FOLDER/trees/NJstm/njstm_$SAMPLE.tree
done
echo
echo "Done with step 5: Making consensus trees using NJstm."
echo
############################################
#STEP6: Make 50% majority rule consensus tree
############################################
echo "Step 6: Making consensus tree from all NJstm trees..."
#STEP 6.1: First concatenate all trees into 1 file
cat $INPUT_FOLDER/trees/NJstm/njstm_* > $INPUT_FOLDER/trees/NJstm/all_njstm_trees.txt
#STEP 6.2: Now make a 50% majority rule consensus tree using RAxML
raxml -f b -m GTRGAMMA -J MR -z $INPUT_FOLDER/trees/NJstm/all_njstm_trees.txt -n njstm_consensus -w $INPUT_FOLDER/trees/NJstm/ -T $THREADS
echo
echo "Done with step 6: Making consensus tree from all NJstm trees."
echo
################################################################################################################################
#EXTRA: Count the number of files (=number of loci) and the number of invariate sites by looking at the first line in the phylip files
################################################################################################################################

FILES=($INPUT_FOLDER/fasta_files/*_SNPs.phy)
for f in "${FILES[@]}" 
do 
SAMPLE=`basename $f _SNPs.phy`
LOCI=$((LOCI+1))
SITES=`head -n 1 $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs.phy | cut -d ' ' -f2`
COUNTS=$((COUNTS+SITES))
#rm $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta.phy		#remove intermediate phylip file
done
echo
echo "Done with analysis. $LOCI loci containing $COUNTS variable sites were included."

echo
date
