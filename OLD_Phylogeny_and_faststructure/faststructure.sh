#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script is used to run a FASTSTRUCTURE analysis of Cryphonectria GBS data.
# It consists of the following steps:
# Step 1: convert the genotypes file to a concatenated fasta file
# Step 2: convert alignment to phylip format and remove invariant SNPs (which are there if you made a subselection of samples to be included in your analysis)
# Step 3: convert invariante SNPs to filtered alignment and VCF file. SNPs with more than 2 alleles and SNPs with ambiguous bases are filtered.
# Step 4: file conversion, first to PED and MAP files using VCFtools, then to a PLINK BED file using PLINK
# Step 5: Run faststructure for different values of K
#####################################################

###################
#PARAMETER SETTINGS
###################
#Specify here the folder where all scripts needed for this analysis are:
SCRIPTS_FOLDER=/home/genomics/ahaegeman/GBS_Cryphonectria/scripts
#Specify here the folder where your genotypes file is:
INPUT_FOLDER=/home/genomics/ahaegeman/GBS_Cryphonectria/faststructure_GTRCAT-V_without_5028_sharedall
#Specify here the name of your genotypes file:
GENOTYPES_FILE=genotypes_sharedloci.txt
#Specify the number of K values you want to test in Faststructure (number of populations
K=15

date
echo

######################################################
#STEP1: convert genotypes file to concatenated fasta file
######################################################
echo "Step 1: convert genotypes file to concatenated fasta file..."

#change directory to folder where the data is
cd $INPUT_FOLDER
#Run perl script to create fasta files for each locus. These are stored in a newly created folder called "fasta_files".
#The script also creates a mappingfile.txt, this file maps the names of the alleles to the isolate names and will be used for NJstm.
perl $SCRIPTS_FOLDER/convert_genotypes_to_concatenated_alignment.pl $GENOTYPES_FILE concatenated_alignment.fasta

echo
echo "Done with step 1: converting genotypes file to concatenated fasta file."
echo

##############################
#STEP2: remove invariante SNPs
##############################

#STEP 2.1: MAKE PHYLIP FILES
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d $INPUT_FOLDER/concatenated_alignment.fasta -getPhylip
echo "Done making phylip file from concatenated alignment fasta file."

#STEP 2.2: USE extractVariantSites.py SCRIPT TO REMOVE INVARIANT SITES FROM PHYLIP FILES, see https://github.com/btmartin721/extractVariantSites4raxml
python3 $SCRIPTS_FOLDER/extractVariantSites.py -p $INPUT_FOLDER/concatenated_alignment.fasta.phy -l -o $INPUT_FOLDER/concatenated_alignment_SNPs
echo "Done removing invariant files from concatenated alignment."
echo
echo "Done with step 2: removing invariant SNPs in all fasta files."
echo

##################################################################
#STEP3: convert invariante SNPs to filtered alignment and VCF file
##################################################################
echo "Step 3: filtering alignment for biallelic SNPs and converting to VCF file..."
perl $SCRIPTS_FOLDER/convert_SNPalignment_to_01_VCF_file.pl $INPUT_FOLDER/concatenated_alignment_SNPs.phy $INPUT_FOLDER/concatenated_alignment_SNPs.vcf
echo
echo "Done with step 3: filtering alignment for biallelic SNPs and converting to VCF file."
echo

#######################
#STEP4: File conversion
#######################
echo "Step 4: converting files..."
vcftools --vcf $INPUT_FOLDER/concatenated_alignment_SNPs.vcf --plink --out $INPUT_FOLDER/concatenated_alignment_SNPs
/usr/local/bioinf/plink-1.07-x86_64/plink --file $INPUT_FOLDER/concatenated_alignment_SNPs --make-bed --out $INPUT_FOLDER/concatenated_alignment_SNPs --noweb
echo
echo "Done with step 4: file conversion."
echo

#############################################
#STEP5: Run faststructure for all values of K
#############################################
echo "Step 5: Running faststructure..."

mkdir $INPUT_FOLDER/faststructure

#loop over all K values and run faststructure for all of them
count=1
while [ $count -lt `expr $K + 1` ]
do
  python /usr/local/bioinf/fastStructure/structure.py -K $count --input=$INPUT_FOLDER/concatenated_alignment_SNPs --output=$INPUT_FOLDER/faststructure/concatenated_alignment_SNPs --full --seed=12345
  count=`expr $count + 1`
done

#choose best value for K
python /usr/local/bioinf/fastStructure/chooseK.py --input=$INPUT_FOLDER/faststructure/concatenated_alignment_SNPs > $INPUT_FOLDER/faststructure/chooseK.txt

echo
echo "Done with step 5: Running faststructure."
echo

