#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script is used to calculate a phylogenetic tree from the output of Gibpss.
# It consists of the following steps:
# Step 1: convert the genotypes file to a concatenated fasta file
# Step 2: convert alignment to phylip format and remove invariant SNPs (which are there if you made a subselection of samples to be included in your analysis)
# Step 3: make phylogenetic tree using RAXML, with GTRCAT, no rate heterogeneity and with ascertainment bias. (if you want to use GTRGAMMA, put the GTRCAT in comment and uncomment the GTRGAMMA command).
#####################################################

###################
#PARAMETER SETTINGS
###################
#Specify here the folder where all scripts needed for this analysis are:
SCRIPTS_FOLDER=/home/genomics/ahaegeman/GBS_Cryphonectria/scripts
#Specify here the folder where your genotypes file is:
INPUT_FOLDER=/home/genomics/ahaegeman/GBS_Cryphonectria/filtered_SNP_tree_GTRCAT-V_without_5028_sharedall
#Specify here the name of your genotypes file:
GENOTYPES_FILE=genotypes_sharedloci.txt
#Specify the number of threads you want to use while running RAXML
THREADS=12
#specify the number of bootstraps you want to do
BS=100

date
echo

######################################################
#STEP1: convert genotypes file to concatenated fasta file
######################################################
echo "Step 1: convert genotypes file to concatenated fasta file..."

#change directory to folder where the data is
cd $INPUT_FOLDER
#Run perl script to create fasta files for each locus. These are stored in a newly created folder called "fasta_files".
#The script also creates a mappingfile.txt, this file maps the names of the alleles to the isolate names and will be used for NJstm.
perl $SCRIPTS_FOLDER/convert_genotypes_to_concatenated_alignment.pl $GENOTYPES_FILE concatenated_alignment.fasta

echo
echo "Done with step 1: converting genotypes file to concatenated fasta file."
echo
##############################
#STEP2: remove invariante SNPs
##############################

#STEP 2.1: MAKE PHYLIP FILES
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d $INPUT_FOLDER/concatenated_alignment.fasta -getPhylip
echo "Done making phylip file from concatenated alignment fasta file."

#STEP 2.2: USE extractVariantSites.py SCRIPT TO REMOVE INVARIANT SITES FROM PHYLIP FILES, see https://github.com/btmartin721/extractVariantSites4raxml
python3 $SCRIPTS_FOLDER/extractVariantSites.py -p $INPUT_FOLDER/concatenated_alignment.fasta.phy -l -o $INPUT_FOLDER/concatenated_alignment_SNPs
echo "Done removing invariant files from concatenated alignment."
echo
echo "Done with step 2: removing invariant SNPs in all fasta files."
echo

########################
#STEP3: filter alignment
########################
#the perl script takes as input the SNP alignment in phylip format.
#it then filters out the sites with > 2 alleles, and also the sites with ambiguous characters (more than 2 alleles)
#output is a filtered alignment in fasta format, as well as a table with 0/1 data which can be analyzed using R (distance matrix and hierarchical clustering).
perl $SCRIPTS_FOLDER/convert_SNPalignment_to_01.pl $INPUT_FOLDER/concatenated_alignment_SNPs.phy $INPUT_FOLDER/concatenated_alignment_SNPs_01_filtered.txt
echo
echo "Done with step 3: filtering alignment for biallelic unambiguous SNPs."
echo
##############################
#STEP4: Make phylogenetic tree, including 100 bootstraps
##############################
echo "Running RAXML ..."
#GTRCAT (fast version) without rate heterogeneity (-V)
#tree without filtering
raxml -m ASC_GTRCAT -V --asc-corr=lewis -s $INPUT_FOLDER/concatenated_alignment_SNPs.phy -p 12345 -x 12345 -n concatenated_SNPs -T $THREADS -w $INPUT_FOLDER -f a -# $BS >> raxml_log.txt
#tree with filtering
raxml -m ASC_GTRCAT -V --asc-corr=lewis -s $INPUT_FOLDER/concatenated_alignment_SNPs.phy_filtered.fasta -p 12345 -x 12345 -n concatenated_SNPs_filtered -T $THREADS -w $INPUT_FOLDER -f a -# $BS >> raxml_log.txt

#GTRGAMMA (slow more accurate version) with rate heterogeneity
#raxml -m ASC_GTRGAMMA --asc-corr=lewis -s $INPUT_FOLDER/concatenated_alignment_SNPs.phy -p 12345 -x 12345 -n concatenated_SNPs -T $THREADS -w $INPUT_FOLDER -f a -# $BS >> raxml_log.txt
echo "Done running RAXML on concatenated SNP trees."

############################################################################################
#EXTRA: Count the number of invariate sites by looking at the first line in the phylip files
############################################################################################

COUNTS=`head -n 1 $INPUT_FOLDER/concatenated_alignment_SNPs.phy | cut -d ' ' -f2`
COUNTS2=`tail -n 1 $INPUT_FOLDER/concatenated_alignment_SNPs.phy_filtered.fasta | wc -c`
echo
echo "Done with analysis. $COUNTS variable sites were included in the non filtered alignment, $COUNTS2 sites were included in the filtered alignment."

echo
date
