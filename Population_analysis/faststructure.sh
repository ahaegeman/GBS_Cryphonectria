#!/bin/bash
# (c) Annelies Haegeman 2020
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script is used to run a FASTSTRUCTURE analysis of Cryphonectria GBS data.
# Make sure to activate the faststructure environment using conda activate before you run this script.
# It consists of the following steps:
# Step 1: convert concatenated alignment file in fasta format to filtered alignment and VCF file. Only biallelic SNPs are kept (the allele can also be a gap or an ambiguous base).
# Step 2: file conversion, first to PED and MAP files using VCFtools, then to a PLINK BED file using PLINK
# Step 3: Run faststructure for different values of K
#####################################################

###################
#PARAMETER SETTINGS
###################
#Specify here the folder where all scripts needed for this analysis are:
SCRIPTS_FOLDER=/home/genomics/ahaegeman/GitLab/GBS_Cryphonectria/Faststructure
#Specify here the input alignment file including its full path:
INPUT_FILE=/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Gibpss_6/test2/filtered_alignment_a210_g20_reordered_renamed_wooutgroup.fasta
#Specify here the folder where all output needs to be stored:
OUTPUT_FOLDER=/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Gibpss_6/test2
#Specify the number of K values you want to test in Faststructure (number of populations)
K=30

date
echo

########################################################################
#STEP1: filter alignment for biallelic SNPs only and convert to VCF file
########################################################################
echo "Step 1: filtering alignment for biallelic SNPs only and converting to VCF file..."
perl $SCRIPTS_FOLDER/convert_SNPalignment_to_01_VCF_file.pl $INPUT_FILE $OUTPUT_FOLDER/filtered_alignment.vcf
echo
echo "Done with step 1: filtering alignment for biallelic SNPs and converting to VCF file."
echo

#######################
#STEP2: File conversion
#######################
echo "Step 2: converting files..."
vcftools --vcf $OUTPUT_FOLDER/filtered_alignment.vcf --plink --out $OUTPUT_FOLDER/filtered_alignment
plink --file $OUTPUT_FOLDER/filtered_alignment --make-bed --out $OUTPUT_FOLDER/filtered_alignment --noweb
echo
echo "Done with step 2: file conversion."
echo

#############################################
#STEP3: Run faststructure for all values of K
#############################################
echo "Step 3: Running faststructure..."

mkdir $OUTPUT_FOLDER/faststructure

#loop over all K values and run faststructure for all of them
count=1
while [ $count -lt `expr $K + 1` ]
do
  #run faststructure
  structure.py -K $count --input=$OUTPUT_FOLDER/filtered_alignment --output=$OUTPUT_FOLDER/faststructure/filtered_alignment --full --seed=12345
  #change to output folder to make the distruct files and run distruct
  cd $OUTPUT_FOLDER/faststructure
  #run perl script to make files for distruct
  perl $SCRIPTS_FOLDER/make_files_for_distruct.pl $INPUT_FILE filtered_alignment.$count.meanQ meanQ.txt labels.txt drawparams
  #run distruct
  /home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Gibpss_6/test/distruct1.1/distructLinux1.1 -K $count -o "faststructure_distruct_K"$count".ps"
  #convert .ps file to PDF file with ps2pdfwr
  ps2pdfwr "faststructure_distruct_K"$count".ps" "faststructure_distruct_K"$count".pdf"
  #add +1 to counter
  count=`expr $count + 1`
done

#choose best value for K
chooseK.py --input=$OUTPUT_FOLDER/faststructure/filtered_alignment > $OUTPUT_FOLDER/faststructure/chooseK.txt

echo
echo "Done with step 3: Running faststructure."
echo

