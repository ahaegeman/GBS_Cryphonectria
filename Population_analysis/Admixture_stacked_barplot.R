#################
#Load libraries
library(ggplot2)
library(reshape2) #for melt

#################
#CHANGE DATA DIRECTORY
#setwd("/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Variant_analysis/Admixture_n2")
setwd("/home/genomics/ahaegeman/GBS_Cryphonectria/SMAP_jan2021/5_variant_filtering_and_analysis/Admixture")

#################
#READ DATA

#SET NUMBER OF POPULATIONS TO MAKE THE GRAPHS FOR
K<-13

#READ INDIVIDUAL NAMES FROM TXT FILE
#First make a file from the vcf file, containing all individual names:
# head -n 70 Cryphonectria_variants_RAGY01_n2_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.recode.vcf | tail -n 1 > headers.txt
# In notepad ++, replace tabs by enters, remove excess column names, clean the individual names and save resulting file as "samplenames.txt"
# Rename some individuals: add "_2" to them because they are a replicate from Run16: 5062, 5089, M6563, M6574, M6618, M6662, UK30, UK31
inds.df<-read.table(file="samplenames.txt",sep="\t",header=F)
inds.list<-inds.df$V1

#READ Q FILE (OUTPUT ADMIXTURE)
#read file
#qvalues<-read.table(paste0("Cryphonectria_variants_RAGY01_n2_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.",K,".Q"),sep=" ")
qvalues<-read.table(paste0("Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.",K,".Q"),sep=" ")
#rename the columns
colnames(qvalues)<-sub('V', 'pop', colnames(qvalues)) 
#rename rows using the individual names extracted from the fasta file
rownames(qvalues)<-inds.list
#add column to qvalues with individual names
qvalues$samplename<-inds.list

#READ INDIVIDUAL NAMES OF SAMPLES TO BE KEPT IN CORRECT ORDER TO SHOW ON THE GRAPH (M6370_1 is removed, since there is already a replicate in there, M6370_2)
inds.ordered<-read.table("samples_to_be_kept_in_correct_order.txt",sep="\t",header=T)
qvalues.selected<-qvalues[qvalues$samplename %in% inds.ordered$samplename,]
qvalues.selected<-merge(qvalues.selected,inds.ordered, by="samplename", all.x=TRUE) #merge the two dataframes
qvalues.selected$samplename<-factor(qvalues.selected$samplename, levels=inds.ordered$samplename) #to change the order of the samples in the graph
qvalues.selected$samplename_long<-factor(qvalues.selected$samplename_long, levels=inds.ordered$samplename_long)

#################
#STACKED BAR GRAPH

#First make a long version of the qvalues table
qvalues.selected$samplename<-NULL
qvalues.selected$inds<-NULL
qvalues.melt<-melt(qvalues.selected, id.vars="samplename_long",variable.name = "population")

pdf(file=paste0("Admixture_Mapping_VC_n1_SMAPloci_maxalleles2_minmeanDP10_maxmissing0.9_K",K,".pdf"),width=11.27,height=8.27)  #A4 size landscape in inches
ggplot(qvalues.melt, aes(x = samplename_long, y = value, fill = population)) + 
  geom_bar(stat = "identity", width=1) +   # (100-width) is the percentage of whitespace between the bars, with width=0.6 you have some whitespace between the bars, with width=1 none.
  ylab("percentage") +
  scale_y_continuous(expand = c(0,0)) + # to eliminate space between bar plot and labels
  #scale_fill_grey(start = 0.8, end = 0.1) + #use greyscale to fill bars
  theme_bw() + #use this theme to remove the grey background
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5, size=4)) #adjust X-axis labels  
dev.off()

#################
#################
#SIMILAR SCRIPT, BUT NOW START FROM GIBPSS ALIGNMENT ADMIXTURE OUTPUT

#READ INDIVIDUAL NAMES FROM FASTA FILE
#First read fasta file line per line
fasta<-readLines("/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Gibpss_6/admixture_a210_g20/filtered_alignment_a210_g20_reordered_renamed_wooutgroup.fasta")
#Identify header lines
headerlines<-grep(">", fasta)
#store contents of headerlines in vector of individuals, and remove the ">" sign
inds<-sub('>', '', fasta[headerlines])
#remove individual 194 (second occurrence of NEB014C_2_FRA_RE019, isolate 194 in the list, should be left out of the analysis)
inds<-inds[-194]

#READ Q FILE (OUTPUT ADMIXTURE)
#read file
qvalues<-read.table(paste0("/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Gibpss_6/admixture_a210_g20/filtered_alignment.",K,".Q"),sep=" ")
#rename the columns
colnames(qvalues)<-sub('V', 'pop', colnames(qvalues)) 
#remove the individual which should be left out the analysis (second occurrence of NEB014C_2_FRA_RE019, isolate 194 in the list)
qvalues<-qvalues[-(194),] #remove row 194
#rename rows using the individual names extracted from the fasta file
rownames(qvalues)<-inds
#add column to qvalues with individual names
qvalues$samplename_long<-inds

#READ INDIVIDUAL NAMES OF SAMPLES TO BE KEPT IN CORRECT ORDER TO SHOW ON THE GRAPH (M6370_1 is removed, since there is already a replicate in there, M6370_2)
inds.ordered<-read.table("samples_to_be_kept_in_correct_order.txt",sep="\t",header=T)
qvalues.selected<-qvalues[qvalues$samplename_long %in% inds.ordered$samplename_long,]
qvalues.selected$samplename_long<-factor(qvalues.selected$samplename_long, levels=inds.ordered$samplename_long)

#MAKE PLOT
#First make a long version of the qvalues table
qvalues.melt<-melt(qvalues.selected, id.vars="samplename_long",variable.name = "population")

pdf(file=paste0("Admixture_Gibpss_alignment_a210_g20_K",K,".pdf"),width=11.27,height=8.27)  #A4 size landscape in inches
ggplot(qvalues.melt, aes(x = samplename_long, y = value, fill = population)) + 
  geom_bar(stat = "identity", width=1) +   # (100-width) is the percentage of whitespace between the bars, with width=0.6 you have some whitespace between the bars, with width=1 none.
  ylab("percentage") +
  scale_y_continuous(expand = c(0,0)) + # to eliminate space between bar plot and labels
  #scale_fill_grey(start = 0.8, end = 0.1) + #use greyscale to fill bars
  theme_bw() + #use this theme to remove the grey background
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5, size=4)) #adjust X-axis labels  
dev.off()
