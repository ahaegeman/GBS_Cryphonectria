#!/usr/bin/perl
#this script makes the accompanying files to visualize a faststructure analysis by the software "distruct".
#It uses a fasta alignment, and a meanQ file (as made by faststructure) as input
#the output is a "drawparams" file, a reformatted meanQ file and a labels file, which all together can be used to run "distruct".

$|=1;
use strict;
#use warnings;
use Bio::SeqIO;


#############
#input files#
#############
my $inputfile= $ARGV[0];
my $meanQfile= $ARGV[1];
my $outmeanQfile=$ARGV[2];
my $labelsfile=$ARGV[3];
my $drawparamsfile=$ARGV[4];
die "usage perl make_files_for_distruct.pl alignment.fasta meanQ.txt output_meanQ.txt output_labels.txt drawparams" if (scalar @ARGV != 5);


#####################
#initiate variabeles#
#####################
my ($seqio1,$sequence_object1,$id,$line,$drawparams);
my $i=0;
my $j=0;
open(OP1,">$outmeanQfile");
open(OP2,">$labelsfile");
open(OP3,">$drawparamsfile");


########
#SCRIPT#
########

#read meanQ file and print reformatted meanQ file to output
open (IP,$meanQfile) || die "cannot open \"$meanQfile\":$!";
while ($line=<IP>)		
	{  	chomp $line;
		$j++;	#count the line number
		$line =~ s/\s{2}/\t/g;  #replace the double spaces from the meanQ file by tabs
		print OP1 "$j",":","\t","$line","\t","1","\n";
	}


#read fasta file and print labels of each sequence to a labels file
$seqio1 = Bio::SeqIO -> new('-format' => 'fasta','-file' => $inputfile);
while ($sequence_object1 = $seqio1 -> next_seq) {
	$i++; #counter for sequence number
	$id = $sequence_object1->id();

	print OP2 "$i\t$id\n";
}

#write drawparams file by storing the block of text in $drawparams, and printing that variable to the $drawparamsfile at the end.
$drawparams = <<"ENDOFFILE";

PARAMETERS FOR THE PROGRAM distruct.  YOU WILL NEED TO SET THESE
IN ORDER TO RUN THE PROGRAM.  

"(int)" means that this takes an integer value.
"(B)"   means that this variable is Boolean 
        (1 for True, and 0 for False)
"(str)" means that this is a string (but not enclosed in quotes) 
"(d)"   means that this is a double (a real number).

Data settings

#define INFILE_POPQ        $outmeanQfile      // (str) input file of population q's
#define INFILE_INDIVQ
#define INFILE_LABEL_BELOW
#define INFILE_LABEL_ATOP	$labelsfile
#define INFILE_CLUST_PERM 
#define OUTFILE                  //(str) name of output file

#define K     // (int) number of clusters	
#define NUMPOPS $i    // (int) number of pre-defined populations
#define NUMINDS $i  // (int) number of individuals

Main usage options

#define PRINT_INDIVS      0  // (B) 1 if indiv q's are to be printed, 0 if only population q's
#define PRINT_LABEL_ATOP  1  // (B) print labels above figure
#define PRINT_LABEL_BELOW 0  // (B) print labels below figure
#define PRINT_SEP         0  // (B) print lines to separate populations

Figure appearance

#define FONTHEIGHT 2.8	// (d) size of font
#define DIST_ABOVE 4	// (d) distance above plot to place text
#define DIST_BELOW -7	// (d) distance below plot to place text
#define BOXHEIGHT  150	// (d) height of the figure
#define INDIVWIDTH 2.8	// (d) width of an individual


Extra options

#define ORIENTATION 0	     // (int) 0 for horizontal orientation (default)
			     //       1 for vertical orientation
			     //	      2 for reverse horizontal orientation
                             //       3 for reverse vertical orientation
#define XORIGIN 15		// (d) lower-left x-coordinate of figure
#define YORIGIN 288		// (d) lower-left y-coordinate of figure
#define XSCALE 1		// (d) scale for x direction
#define YSCALE 1		// (d) scale for y direction
#define ANGLE_LABEL_ATOP 90	// (d) angle for labels atop figure (in [0,180])
#define ANGLE_LABEL_BELOW 60    // (d) angle for labels below figure (in [0,180])
#define LINEWIDTH_RIM  3	// (d) width of "pen" for rim of box
#define LINEWIDTH_SEP 0.3	// (d) width of "pen" for separators between pops and for tics
#define LINEWIDTH_IND 0.3	// (d) width of "pen" used for individuals 
#define GRAYSCALE 0	        // (B) use grayscale instead of colors
#define ECHO_DATA 1             // (B) print some of the data to the screen
#define REPRINT_DATA 1          // (B) print the data as a comment in the ps file
#define PRINT_INFILE_NAME 0     // (B) print the name of INFILE_POPQ above the figure 
                                //     this option is meant for use only with ORIENTATION=0 
#define PRINT_COLOR_BREWER 1    // (B) print ColorBrewer settings in the output file 
                                //     this option adds 1689 lines and 104656 bytes to the output
                                //     and is required if using ColorBrewer colors


Command line options:

-d drawparams
-K K
-M NUMPOPS
-N NUMINDS
-p input file (population q's)
-i input file (individual q's)
-a input file (labels atop figure)
-b input file (labels below figure)
-c input file (cluster permutation)
-o output file


ENDOFFILE

print OP3 $drawparams;

