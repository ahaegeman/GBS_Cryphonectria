#!/usr/bin/perl
#this script converts an alignment in fasta format.
#it then filters out the SNPs with more than 2 characters (a gap and an aberrant base count as a character)
#finally it converts the two characters to 1 (most frequent) and 0 and writes the data into a VCF formatted file (haploid data only)

$|=1;
use strict;
#use warnings;
use List::Util qw( reduce );
use Bio::SeqIO;

#############
#input files#
#############
my $datafile= $ARGV[0];
my $outputfile=$ARGV[1];
die "usage perl convert_SNPalignment_to_01.pl alignment.fasta output.vcf" if (scalar @ARGV != 2);

#####################
#initiate variabeles#
#####################
my ($seqio1,$sequence_object1,$lijn,@line,$numberofsamples,$length,@metrics,$sample,$seq,%data_per_position,%data_per_sample,%filtered_data_per_sample,@samples,%strings,@keep,$check,$element,@discard,$uniq,$position,%headcharacter,%secondcharacter,%count,$max_val_key);
my $i=0;
my $j=0;
my $k=0;
my $l=0;
my $m=0;
my $n=0;
my $aberrant=0; 
my $numberofsamples=0;
open(OP1,">".$datafile."_filtered.fasta");
open(OP2,">$outputfile");

########
#SCRIPT#
########

#read fasta file and store all bases in a hash with as keys the sample ID and the position in the alignment
$seqio1 = Bio::SeqIO -> new('-format' => 'fasta','-file' => $datafile);
while ($sequence_object1 = $seqio1 -> next_seq) {
	$seq = $sequence_object1->seq();
	$sample = $sequence_object1->id();
	push (@samples,$sample);	#store sample names in array
	
	#store sequence data_per_position per sample in a hash with as key the sample ID and as value the sequence
	$data_per_sample{$sample}=$seq;
	
	$length = length($seq); #length of sequence
	$numberofsamples++;  #total number of sequences
	
	#loop over all bases of the sequence and store the corresponding base in a hash with keys the sample name and the base number
	while ($i<$length){
		$data_per_position{$sample}{$i+1}=substr($seq, $i, 1); 
		$i++;
		}
	$i=0;	
}


#now loop over each position in the alignment and store all characters of a certain position in a string
while ($l < $length){
	foreach $sample (@samples){
		$strings{$l+1}=$strings{$l+1}.$data_per_position{$sample}{$l+1}	#hash with as key the position in the alignment, and as value a string of all observed bases for that position over all samples
	}
	$l++;
}

#check
#while ($k < $length){
#	print "$strings{$k+1}\n";
#	$k++;
#}

#loop over all positions, check the string and determine whether the position should be kept or discarded (only keep when there are 2 different states) + check the most occurring character in the string
while ($k < $length){
	#$aberrant=$strings{$k+1}=~ /\-|N|R|Y|S|W|K|M|B|D|H|V/i; #check for aberrant characters, if present, $aberrant will have value 1.
	$uniq=number_of_uniques($strings{$k+1});	#check the number of different/unique bases. Should be exactly 2 if we only keep biallelic SNPs.
	#if ($aberrant > 0 or $uniq != 2 ){			#to discard = $aberrant>0, $uniq != 2
	if ($uniq == 2 ){			#to keep: only positions with exactly two states = biallelic SNPs. These SNPs are allowed to be aberrant characters or gaps.
		push(@keep,$k+1);	#add position in alignment to array of positions to keep
	}
	else{
		push(@discard,$k+1);		#add position in alignment to array of positions to discard
	}
	#print "$strings{$k+1}\t$aberrant\t$uniq\n";	#check
	
	#check which character occurs most for each position and store info in hash %headcharacter with as key the position in the alignment and as value the most occurring character
	#to do this, first store counts of each character in a hash
	foreach my $str (split //, $strings{$k+1}) {
		$count{$str}++;
		}
	#now find the key of the hash with the maximum value
	$headcharacter{$k+1} = reduce { $count{$a} > $count{$b} ? $a : $b } keys %count;	#find key for which the value is the maximum of the complete hash
	$secondcharacter{$k+1} = reduce { $count{$a} <= $count{$b} ? $a : $b } keys %count;	#find key for which the value is less than the maximum of the complete hash
	#print "$k+1\t$strings{$k+1}\t$headcharacter{$k+1}\t$secondcharacter{$k+1}\n";	
	#make hash with counts empty
	%count=();
	
	#reset variables
	$k++;
	#$aberrant=0;	
}


#check
#foreach $element (@keep){
#	print "$element\t";
#	print "$headcharacter{$element}\n";
#}

#initiate VCF file and print header with sample names
print OP2 "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t";
while ($n < $numberofsamples){
	print OP2 "$samples[$n]\t";
	$n++;
}
print OP2 "\n";

#loop over all positions to keep and make a new file in VCF format (OP2)
foreach $position (@keep){
	#print all fields with data on that position to the VCF file
	print OP2 "dummy\t";	#chrom
	print OP2 "$position\t";	#pos
	print OP2 ".\t";			#ID
	print OP2 "$headcharacter{$position}\t";	#REF
	print OP2 "$secondcharacter{$position}\t";	#ALT
	print OP2 ".\t";	#QUAL
	print OP2 ".\t";	#FILTER
	print OP2 ".\t";	#INFO
	print OP2 "GT\t";	#FORMAT					
	#loop over samples and add data for each sample to VCF file and make concatenated alignment for fasta file
	while ($m < $numberofsamples){
		$sample=$samples[$m];	
		#check if base on that position is the same or different as the most frequent character at that position and assign 1 or 0 to it
		if (substr($data_per_sample{$sample},$position-1,1) eq $headcharacter{$position}) {
			#print OP2 $position.substr($data_per_sample{$sample},$position-1,1).$headcharacter{$position}." 1\t";
			print OP2 "0\t";
			}
		else {
			#print OP2 $position.substr($data_per_sample{$sample},$position-1,1).$headcharacter{$position}." 0\t";
			print OP2 "1\t";
			}
		$m++;
		}
	$m=0;	
	print OP2 "\n";
}


#loop over all samples to keep and print the concatenated sequences to a new alignment file in fasta format
$m=0;	
while ($m < $numberofsamples){
	$sample=$samples[$m];
	foreach $position (@keep){
		$filtered_data_per_sample{$sample}=$filtered_data_per_sample{$sample}.substr($data_per_sample{$sample},$position-1,1);	#store DNA alignment to keep
	}
	$m++;
	print OP1 ">$sample\n";
	print OP1 "$filtered_data_per_sample{$sample}\n";	
}	


#function to calculate the number of unique characters in a string
sub number_of_uniques {
   my %s;
   return scalar (grep !$s{$_}++, split '', $_[0]);
}

#check
#foreach $sample (@samples){
#	print "$sample\t";
#	while ($j<$length){
#		print "$data_per_position{$sample}{$j+1}";
#		$j++;			
#	}
#	$j=0;
#	print "\n";
#}
