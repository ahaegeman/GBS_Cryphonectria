#!/usr/bin/python3

#===============================================================================
# Description
#===============================================================================

# Elisabeth VEECKMAN december 2014

"""
Script to map reads to a reference genome in parallel using BWA mem.
"""

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime
import multiprocessing

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Map reads of all fastq files in parallel.')

# Add positional arguments (mandatory)
parser.add_argument('reference',
                    help = 'Full path and name of the reference genome')
# Add optional arguments
parser.add_argument('--paired', 
                    dest = 'paired', 
                    action = 'store_true',
                    help = 'Use this option if you have paired end data (default).')
parser.add_argument('--single', 
                    dest = 'paired', 
                    action = 'store_false',
                    help = 'Use this option if you have single end data.')
parser.set_defaults(paired=False)
parser.add_argument('-d', '--dir',
                    default = './',
                    type = str,
                    help = 'Directory containing fastq files (default current directory)')
parser.add_argument('-s', '--suffix',
                    default = '.fq',
                    type = str,
                    help = 'Suffix of fastq files to take into account for mapping. Zipped fastq files are allowed. (default .fq)')
parser.add_argument('-p', '--processes',
                    default = 4,
                    type = int,
                    help = 'Define the number of parallel processes (default 4)')
parser.add_argument('-l', '--log',
                    default = "Map_BWA_parallel.log",
                    type = str,
                    help = 'Indicate the name of the log file (default Map_BWA_parallel.log)')

# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    """
    Print the current date and time to stderr.
    """
    sys.stderr.write('----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return

def commands_paired (basename, dir = args['dir'], suffix = args['suffix'], reference = args['reference'], log = args['log']):
    """
    This function contains all commands needed to map reads to the reference genome.
    """
    
    # map reads to reference genome
    cmd1 = "bwa mem -M -t 16 " + reference + " " + dir + basename + "_1" + suffix + " " + dir + basename + "_2" + suffix + " > " + basename + ".BWA.sam 2>> " + basename + ".BWA.log"
    os.system(cmd1)
    
    # convert sam to sorted bam
    cmd2 = "samtools view -bS " + basename + ".BWA.sam | samtools sort - " + basename + ".BWA"
    os.system(cmd2)
     
    # index bam file
    cmd3 = "samtools index " + basename + ".BWA.bam"
    os.system(cmd3)
     
    # concat sample log to log file
    os.system('cat ' + basename + '.BWA.log >> ' + log)
    os.system('echo \"' + basename + '.BWA.log finished\">> ' + log)
    os.system('rm ' + basename + '.BWA.log')
    
    return

def commands_single (basename, dir = args['dir'], suffix = args['suffix'], reference = args['reference'], log = args['log']):
    """
    """
        
    # map reads to reference genome
    cmd1 = "bwa mem -t 6 -B 1 -v 1 " + reference + " " + dir + basename + suffix + " > " + basename + ".BWA.sam 2>> " + basename + ".BWA.log"
    os.system(cmd1)
    
    # convert sam to sorted bam
    cmd2 = "samtools view -bS " + basename + ".BWA.sam | samtools sort - " + basename + ".BWA"
    os.system(cmd2)
     
    # index bam file
    cmd3 = "samtools index " + basename + ".BWA.bam"
    os.system(cmd3)
     
    # concat sample log to log file
    os.system('cat ' + basename + '.BWA.log >> ' + log)
    os.system('echo \"' + basename + '.BWA.log finished\">> ' + log)
    os.system('rm ' + basename + '.BWA.log')
    
    return

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    
    print_date()
     
    # creating a pool of 4 workers
    with multiprocessing.Pool(args['processes']) as p:

        if args['paired'] == True:
            sys.stderr.write("* Started mapping paired end read files ...\n")
            # build a list of tasks (= all basenames of fastqfiles)
            tasks = [f[:(len(args['suffix'])  * -1) - 2] for f in os.listdir(args['dir']) if f.endswith("_1" + args['suffix'])]
            # run tasks in parallel
            p.map(commands_paired, tasks)
            
        elif args['paired'] == False:
            sys.stderr.write("* Started mapping single end read files ...\n")
            # build a list of tasks (= all basenames of fastqfiles)
            tasks = [f[:(len(args['suffix'])  * -1)] for f in os.listdir(args['dir']) if f.endswith(args['suffix'])]
            # run tasks in parallel
            p.map(commands_single, tasks)
  
    sys.stderr.write('* Finished\n\n')
    
    print_date()