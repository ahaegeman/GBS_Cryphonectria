#!/usr/bin/env Rscript
#set working directory
#setwd("/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Variant_analysis/Analysis_in_R")
setwd("/home/genomics/ahaegeman/GBS_Cryphonectria/SMAP_jan2021/5_variant_filtering_and_analysis/Analysis_in_R")

#install and load required packages
library(dplyr)
library(vcfR) #more info: https://knausb.github.io/vcfR_documentation/index.html
library(ape)
library(vegan) #for jaccard
library(gplots) #for heatmap.2
library(RColorBrewer)
library(adegenet)
library(poppr)
library(pegas)
library(ade4)
library(factoextra)#for mapping dudi.PCA objects
library(SNPRelate)
library(igraph)
library(ggrepel)
library(ggtree)
library(phytools)

#to prevent R from printing results
options(max.print=200)

##########################
#read input files
#vcf <- read.vcfR("/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Variant_analysis/Analysis_in_R/Cryphonectria_variants_RAGY01_n2_maxalleles2_minmeanDP10_maxmissing0.9_2indvsremoved.recode.vcf", verbose = FALSE )
vcf <- read.vcfR("/home/genomics/ahaegeman/GBS_Cryphonectria/SMAP_jan2021/5_variant_filtering_and_analysis/Cparasitica_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9.recode.vcf", verbose = FALSE )
#dna <- ape::read.dna("/home/genomics/ahaegeman/GBS_Verticillium/Genome_Vn/PDEZ01.1.fasta", format = "fasta")
#metadata <- read.table(file="/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Variant_analysis/Analysis_in_R/metadata_for_R.txt",header=T,sep="\t",na.strings=c("","NA","?"),stringsAsFactors = F)
metadata <- read.table(file="/home/genomics/ahaegeman/GBS_Cryphonectria/SMAP_jan2021/5_variant_filtering_and_analysis/Analysis_in_R/metadata_for_R.txt",header=T,sep="\t",na.strings=c("","NA","?"),stringsAsFactors = F)

#check metadata
str(metadata)
#replace "NA" values to "unknown", then this is also a category to include as color when making the graphs
metadata[is.na(metadata)] <- "unknown"
#set some columns in metadata as factor
metadata$Country<-as.factor(metadata$Country)
metadata$Country_code<-as.factor(metadata$Country_code)
metadata$VCtype<-as.factor(metadata$VCtype)
metadata$matingtype<-as.factor(metadata$matingtype)
metadata$SSRgenotype<-as.factor(metadata$SSRgenotype)
metadata$GBSRun<-as.factor(metadata$GBSRun)
metadata$outgroup<-as.factor(metadata$outgroup)

#create chromR object: only works if your DNA file is 1 sequence (not a multifasta)
#chrom <- create.chromR(name="Supercontig", vcf=vcf, seq=dna)
#head(chrom)

##########################
#CHECK VCFR OBJECT AND EXTRACT POLYMORPHIC SITES

vcf
dim(vcf)
head(vcf)
head(is.polymorphic(vcf, na.omit = TRUE))
head(is.biallelic(vcf))
#check "FIX" region of the vcf object
head(getFIX(vcf))
#check part of the genotype section of the vcf object
vcf@gt[1:6, 1:4]
#extract indels
vcf2 <- extract.indels(vcf) #this removes the indels from the vcfR object
#subsetting
biallelic<-vcf[is.biallelic(vcf),]
#extract genotypes (not as numeric, since in diploid calls, it looks like 1/0 or 0/0 or 1/1)
gt <- extract.gt(vcf, element = "GT", as.numeric = FALSE)
#extract depths
dp <- extract.gt(vcf, element = "DP", as.numeric = TRUE)
#check if variants are polymorphic
head(is.polymorphic(vcf, na.omit = TRUE))

#we only keep the polymorphic variants
vcf.poly <- vcf[is.polymorphic(vcf, na.omit = T)]
dim(vcf.poly)
#explore the variants of the genotypes (1 vs 0) 
gt <- extract.gt(vcf.poly, element="GT",as.numeric=TRUE) #in case you used diploid calles, you can put as numeric as FALSE (then you 0/0 or 0/1 or 1/1, not 1 or 0)

#order the metadata file to contain the correct individuals in the correct order as in the vcfR object
colnames(vcf.poly@gt)[-1]
metadata_ordered_to_vcf<-metadata[match(colnames(vcf.poly@gt)[-1], metadata$vcfname),]
rownames(metadata_ordered_to_vcf)<-1:length(colnames(vcf.poly@gt)[-1])
#colnames(vcf.poly@gt)[-1]<-paste0(metadata_ordered$Isolate_code,"_",metadata_ordered$Country,"_",metadata_ordered$Mating_type)
colnames(vcf.poly@gt)[-1]<-as.character(metadata_ordered_to_vcf$samplename_long)


################################
#GENLIGHT OBJECTS, CALCULATE HAMMING DISTANCE, MAKE DISTANCE TREE WITH POPPR
#!!TREES ARE TYPICALLY NOT VERY NICE, RATHER USE RAXML ON ALIGNMENT OF SNPs

#convert vcfR object to genlight object
#please note that the genlight object only accepts loci with max. 2 alleles, make sure you filter out loci with more alleles before converting to genlight object
gl.poly <- vcfR2genlight(vcf.poly)
#set ploidy (in this case 1)
ploidy(gl.poly) <- 1
#check individuals
gl.poly@ind.names
#set populations, in this case the countries
pop(gl.poly)<-metadata_ordered_to_vcf$Country_code
#pop(gl.poly)<-rep("pop",times=length(gl.poly@ind.names))  #in case of no data on populations

#calculate distances using "bitwise.dist" from poppr
poly.dist.hamming <- poppr::bitwise.dist(gl.poly) #Hamming distance (number of differences between two strings)
poly.dist.eucl <- poppr::bitwise.dist(gl.poly, euclidean=TRUE) #Euclidean distance

#alternatively, you can immediately use the aboot function of poppr to calculate a dendrogram, with the bitwise.dist as distance function
#in this case we do 100 bootstraps (sample=100)
distancetree<-aboot(gl.poly,tree="upgma",distance=bitwise.dist,sample=100,showtree=FALSE,root=F)
distancetree_rooted<-root(distancetree, outgroup="5028_radicalis_outgroup")

#write tree to pdf and define colors based on the metadata
#define as many colors as there are countries in the metadata and write tree to pdf
# !! REMARK: the colors do not seem correct on the tree, still need to figure out why.
#cols<-brewer.pal(n = length(levels(metadata_ordered_to_vcf$SSRgenotype)), name="Dark2")
library(randomcoloR)
n <- length(levels(metadata_ordered_to_vcf$SSRgenotype))
cols <- distinctColorPalette(n)
pdf(file="Cparasitica_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_hammingdistancetree_colorbySSRgenotype.pdf",width=46.8,height=33.1)
plot.phylo(distancetree_rooted, cex = 1, font = 1, adj = 0, tip.color=cols[pop(gl.poly)])
nodelabels(distancetree_rooted$node.label, adj = c(1.3, -0.5), frame = "n", cex = 1,font = 1, xpd = TRUE)
legend("topleft",legend=levels(metadata_ordered_to_vcf$SSRgenotype),fill=cols,border=FALSE, bty="n", cex=2)
dev.off()
#color by mating type
cols<-c("#990000","#38761d") #red and green
pdf(file="Cparasitica_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_hammingdistancetree_colorbymatingtype.pdf",width=46.8,height=33.1)
plot.phylo(distancetree_rooted, cex = 1, font = 1, adj = 0, tip.color=cols[metadata_ordered_to_vcf$matingtype])
nodelabels(distancetree_rooted$node.label, adj = c(1.3, -0.5), frame = "n", cex = 1,font = 1, xpd = TRUE)
legend("topleft",legend=levels(metadata_ordered_to_vcf$matingtype),fill=cols,border=FALSE, bty="n", cex=2)
dev.off()
#color by country code
n <- length(levels(metadata_ordered_to_vcf$Country_code))
cols <- distinctColorPalette(n)
pdf(file="Cparasitica_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_hammingdistancetree_colorbycountry.pdf",width=46.8,height=33.1)
plot.phylo(distancetree, cex = 1, font = 1, adj = 0, tip.color=cols[metadata_ordered_to_vcf$Country_code])
nodelabels(distancetree$node.label, adj = c(1.3, -0.5), frame = "n", cex = 1,font = 1, xpd = TRUE)
legend("topleft",legend=levels(metadata_ordered_to_vcf$Country_code),fill=cols,border=FALSE, bty="n", cex=2)
dev.off()

#save R object of tree for later (can be read back into R without having to recalculate)
saveRDS(distancetree_rooted,file="distancetree_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_hammingdistance.RDS")


################################
#PRINCIPAL COMPONENT ANALYSIS BY glPCA function on genlight objects

#convert vcfR object to genlight object
#please note that the genlight object only accepts loci with max. 2 alleles, make sure you filter out loci with more alleles before converting to genlight object
gl.poly <- vcfR2genlight(vcf.poly)

#PCA analysis (adegenet)
poly.pca<-glPca(gl.poly,nf=3)

#check percent of variation explained
barplot(100*poly.pca$eig/sum(poly.pca$eig), col = heat.colors(50), main="PCA Eigenvalues")
title(ylab="Percent of variance\nexplained", line = 2)
title(xlab="Eigenvalues", line = 1)

#extract data necessary to make the plot
poly.pca.scores<-as.data.frame(poly.pca$scores)
poly.pca.scores$country<-pop(gl.poly)
poly.pca.scores$matingtype<-metadata_ordered_to_vcf$matingtype
poly.pca.scores$code<-rownames(poly.pca.scores)
poly.pca.scores$SSR<-metadata_ordered_to_vcf$SSRgenotype
poly.pca.scores$Country_code<-metadata_ordered_to_vcf$Country_code

#PCA plot, color by SSR genotype
#cols<-brewer.pal(n = length(levels(metadata_ordered_to_vcf$Country_code)), name="Dark2")
library(randomcoloR)
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$SSRgenotype)))
#put the last color on black (this is the color for "unknown")
cols[length(cols)]<-"#000000"
p <- ggplot(poly.pca.scores, aes(x=PC1, y=PC2, colour=SSR)) +
  #plot using ggplot
  geom_point(size=0.5) +
  geom_text(aes(label=code), size=1) +
  scale_color_manual(values = cols) +
  theme_bw() +
  guides(fill = guide_legend(ncol = 1)) #add this line to force the legend in 1 column
pdf(file="PCA_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_colorbySRRgenotype.pdf")
p
dev.off()

#same plot as above, but color by matingtype
cols<-c("#990000","#38761d","#000000") #red, green and black
p <- ggplot(poly.pca.scores, aes(x=PC1, y=PC2, colour=matingtype)) +
  #plot using ggplot
  geom_point(size=0.5) +
  geom_text(aes(label=code), size=1) +
  scale_color_manual(values = cols) +
  theme_bw() +
  guides(fill = guide_legend(ncol = 1)) #add this line to force the legend in 1 column
pdf(file="PCA_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_colorbymatingtype.pdf")
p
dev.off()

#same plot as above, but color by country
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$Country_code)))
p <- ggplot(poly.pca.scores, aes(x=PC1, y=PC2, colour=Country_code)) +
  #plot using ggplot
  geom_point(size=0.5) +
  geom_text(aes(label=code), size=1) +
  scale_color_manual(values = cols) +
  theme_bw() +
  guides(fill = guide_legend(ncol = 1)) #add this line to force the legend in 1 column
pdf(file="PCA_SMAPloci_variants_maxalleles2_minmeanDP10_maxmissing0.9_colorbycountry.pdf")
p
dev.off()


#plot again, but now using ggrepel package to make the labels readable, but in this case there are too many samples to use this
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$SSRgenotype)))
#put the last color on black (this is the color for "unknown")
cols[length(cols)]<-"#000000"
p <- ggplot(poly.pca.scores, aes(x=PC1, y=PC2, colour=SSR)) +
  #plot using ggplot
  geom_point(size=0.5) +
  geom_text_repel(aes(label=code), size=1) +
  scale_color_manual(values = cols) +
  theme_bw() +
  guides(fill = guide_legend(ncol = 1)) #add this line to force the legend in 1 column
pdf(file="PCA_n2_maxalleles2_minmeanDP10_maxmissing0.9_colorbySRRgenotype.pdf")
p
dev.off()



################################
#CONVERT TO GENIND OBJECT (FOR ADEGENET ANALYSIS)
vcf.poly.genind<-vcfR2genind(vcf.poly)
methods(class = "genind")
#check out data
vcf.poly.genind
vcf.poly.genind@pop    
vcf.poly.genind@ploidy
vcf.poly.genind@type   #codominant markers
#check individual names
indNames(vcf.poly.genind)
##rename based on more descriptive name from metadata
#metadata_ordered<-metadata[match(indNames(genotypes.genind), metadata$sampleID), ]
#metadata_ordered$sampleID
#indNames(genotypes.genind)<-metadata_ordered$description
##make new genind object without the outliers 5-1_0432_C-11_1,5-4_0136_C-9_1,5-2_0275_C-8_1 (1st, 16th and 25th  element)
#genotypes.sel.genind<-genotypes.genind[c(2:15,17:24),]
#indNames(genotypes.sel.genind)




############################################
#IDENTITY BY STATE (SNPRelate), use this to do HIERARCHICAL CLUSTERING and draw a HEATMAP

#read as GDS object
#vcf.fn<-"/home/genomics/ahaegeman/GBS_Cryphonectria/Analyses_april2020/Variant_analysis/Analysis_in_R/Cryphonectria_variants_RAGY01_n2_maxalleles2_minmeanDP10_maxmissing0.9_2indvsremoved_wooutgroup.recode.vcf"
vcf.fn<-"/home/genomics/ahaegeman/GBS_Cryphonectria/SMAP_jan2021/5_variant_filtering_and_analysis/Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.recode.vcf"
snpgdsVCF2GDS(vcf.fn,"Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.gds",method="biallelic.only")
#check gds file
snpgdsSummary("Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.gds")

#identity-by-state analysis
genofile<-snpgdsOpen("Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.gds")
ibs<-snpgdsIBS(genofile,autosome.only=FALSE,remove.monosnp=TRUE,missing.rate=0.10) #max 10% missing data
matrix<-(ibs$ibs)

#order metadata based on IBS matrix
metadata_wo_outgroup<-metadata[!(metadata$id %in% c("5028")),]
metadata_ordered_ibs<-metadata_wo_outgroup[match(ibs$sample.id, metadata_wo_outgroup$vcfname), ]
#for rownames and column names of matrix, use one of the (ordered) metadata columns
rownames(matrix)<-metadata_ordered_ibs$samplename_long
colnames(matrix)<-metadata_ordered_ibs$samplename_long
write.table(round(matrix,4),"ibs_matrix_Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.txt",row.names=T,quote=F,col.names=NA,sep="\t")

#perform multidimensional scaling (2D) (PCA) and save results in dataframe
loc<-cmdscale(1-ibs$ibs,k=2)
x<-loc[,1]; y<-loc[,2]
#set results in dataframe and plot with ggplot
MS.df<-as.data.frame(cbind(x,y))
MS.df$samplename_long<-metadata_ordered_ibs$samplename_long
MS.df$country<-metadata_ordered_ibs$Country_code
MS.df$matingtype<-metadata_ordered_ibs$matingtype
MS.df$SSR<-metadata_ordered_ibs$SSRgenotype
rownames(MS.df)<-rownames(matrix)

#plot multidimensional scaling (PCA) (ggplot scatterplot), color by country
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$Country_code)))
p<-ggplot(MS.df, aes(x=x, y=y, col=country)) + 
  geom_point(size=0.2) +
  geom_text(label=MS.df$samplename_long, size=0.4, hjust=0.5, vjust=2.5) +
  scale_color_manual(values=cols)+
  theme(legend.position="top",legend.title=element_blank())
pdf("MS_2D_plot_Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9_colorbycountry.pdf")
p
dev.off()

#plot multidimensional scaling (PCA) (ggplot scatterplot), color by genotype
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$SSRgenotype)))
#put the last color on black (this is the color for "unknown")
cols[length(cols)]<-"#808080"
p<-ggplot(MS.df, aes(x=x, y=y, col=SSR)) + 
  geom_point(size=0.2) +
  geom_text(label=MS.df$samplename_long, size=0.4, hjust=0.5, vjust=2.5) +
  scale_color_manual(values=cols)+
  theme(legend.position="top",legend.title=element_blank())
pdf("MS_2D_plot_Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9_colorbySSRgenotype.pdf")
p
dev.off()

#plot multidimensional scaling (PCA) (ggplot scatterplot), color by matingtype
cols<-c("#990000","#38761d","#808080") #red, green and grey
p<-ggplot(MS.df, aes(x=x, y=y, col=matingtype)) + 
  geom_point(size=0.2) +
  geom_text(label=MS.df$samplename_long, size=0.4, hjust=0.5, vjust=2.5) +
  scale_color_manual(values=cols)+
  theme(legend.position="top",legend.title=element_blank())
pdf("MS_2D_plot_Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9_colorbymatingtype.pdf")
p
dev.off()

#make distance matrix from IBS matrix by doing (1-x)*100
distance<-as.dist(1-matrix)

#do standard hierarchical clustering of distance matrix
cluster<-hclust(distance, method="average")

#define color palette, reverse it so that low values = low distances are dark, while high values are light
hmcol<-rev(colorRampPalette(brewer.pal(11,"RdYlGn"))(100))

#draw heatmap
pdf(file="heatmap_IBS_hierarchical_clustering_Cparasitica_SMAPloci_variants_nooutgroup_maxalleles2_minmeanDP10_maxmissing0.9.pdf",width=33.1,height=46.8)
heatmap.2(as.matrix(distance),
          key=FALSE, #to remove the color legend
          col=hmcol, #use custom colors
          lhei=c(1.5,10), #relative heigth of top dendrogram vs heatmap itself
          lwid=c(1.5,10), #relative heigth of left dendrogram vs heatmap itself
          margins=c(26,15),
          trace='none', #remove trace lines
          cexRow=0.7,cexCol=0.7, #size of text labels in rows and columns
          labCol = colnames(matrix), #use column names as label (or as.matrix(distance))
          labRow = rownames(matrix),
          cellnote=round(matrix*100,2),notecex=0.3, notecol="white", #add values to the cells of the heatmap
          Colv =as.dendrogram(cluster), #use custom dendrogram
          Rowv=as.dendrogram(cluster) #use custom dendrogram          
)
dev.off()

# 
# #INFER GROUPS BY PERMUTATION OF CLUSTERING RESULTS OF IBS MATRIX
# #http://corearray.sourceforge.net/tutorials/SNPRelate/#identity-by-state-analysis
# #perform cluster analysis on IBS distance matrix again (same as hclust average)
# ibs.hc <- snpgdsHCluster(ibs)
# # Determine groups of individuals automatically
# rv <- snpgdsCutTree(ibs.hc)
# #check order and rename samples to include country and status
# table(rv$samp.group) #number of samples in each group
# permutationgroups.df<-as.data.frame(cbind(rv$sample.id,rv$samp.group))
# colnames(permutationgroups.df)<-c("id","group")
# #write table to output
# write.table(permutationgroups.df,"permutationgroups.txt",row.names=T,quote=F,col.names=NA,sep="\t")
# 
# #plot dendrogram
# pdf(file="dendrogram_IBS_UPGMA_groupsbypermutation.pdf",widt=46.8,height=33.1)
# plot(rv$dendrogram)
# dev.off()


# #######################################
# #EXTRA ANALYSES (neighbour joning, upgma based on hamming distance, densitree based on multiple UPGMA trees, minimum spanning network)
# #In our case (extremely similar strains), these extra analyses were not that useful and/or informative.
# 
# #make neighbour joining tree based on IBS distance matrix (should be better than UPGMA because no assumptions on molecular clock)
# njtree = nj(dist.gene(matrix))
# myBoots <- boot.phylo(njtree, matrix, function(xx) nj(dist.gene(xx)), B = 100,  mc.cores = 6)
# #plot NJ tree to pdf, add color bar based on country
# pdf(file="IBS_NJtree_BS.pdf",width=33.1,height=46.8)
# plot(stree, label.offset=0.75, )
# nodelabels(round(myBoots),adj = 0.7,cex=0.5)
# #tiplabels(pch=21, col="black", adj=1, bg=colors[metadata_ordered$status], cex=2) #not used since color order is not correct
# dev.off()
# 
# ################################
# #UPGMA TREE
# 
# #calculate UPGMA tree from hamming distance
# poly.upgma<-phangorn::upgma(bitwise.dist(gl.poly))
# # build a ggplot with a geom_tree
# cols<-brewer.pal(n = length(levels(metadata_ordered$country)), name="Dark2")
# tree<-ggplot(poly.upgma) + geom_tree() + theme_tree() +
#   #geom_tippoint(aes(color=cols[metadata_ordered$country])) + 
#   geom_tiplab(size=1)
# pdf(file='UPGMAtree_Hammingdistance.pdf')
# tree
# dev.off()
# 
# 
# ################################
# #MULTIPLE UPGMA TREES ON SUBSETS OF 500 VARIANTS
# 
# #Next, we will make multiple UPGMA trees with different subsets of the variants
# #These subsets should give similar results, and by using "densitree" we can visualize all trees together.
# 
# # Creating a list object to save our subsets in.
# vcf.poly.subset <- vector(mode = "list", length = 50)
# 
# # Using a for loop to generate 50 subsets of 500 random variants from the rubi.VCF vcfR object.
# for (i in 1:50){
#   vcf.poly.subset[[i]] <- vcf.poly[sample(size = 500, x= c(1:nrow(vcf.poly)))]
# }
# 
# # Checking we have 50 vcfR objects:
# length(vcf.poly.subset)
# head(vcf.poly.subset, n=2)
# 
# # Creating the GenLight object
# gl.poly.subset <- lapply(vcf.poly.subset, function (x) suppressWarnings(vcfR2genlight(x)))
# for (i in 1:length(gl.poly.subset)){
#   ploidy(gl.poly.subset[[i]]) <- 2
# }
# 
# # Creating a simple UPGMA tree per object
# poly.trees <- lapply(gl.poly.subset, function (x) phangorn::upgma(bitwise.dist(x)))
# class(poly.trees) <- "multiPhylo"
# 
# #Calculate an average tree (using Robinson-Foulds distance) and use this tree as order for the "densitree" (next command) (averageTree from phytools package)
# poly.average.tree<-averageTree(poly.trees,method="symmetric.difference")
# #check if order of samples is the same as in metadata_ordered
# match(poly.average.tree$tip.label,paste0(metadata_ordered$code,"_",metadata_ordered$country,"_",metadata_ordered$status))
# 
# #Plot average tree
# average.tree<-ggplot(poly.average.tree) + geom_tree() + theme_tree() +
#   #geom_tippoint(aes(color=cols[metadata_ordered$country])) + 
#   geom_tiplab(size=1)
# pdf(file='averagetree_UPGMA_50randomsubsets500variants.pdf')
# average.tree
# dev.off()
# 
# # Making a visual representation of all trees (by overlapping)
# pdf(file="densitree_UPGMA_50randomsubsets500variants.pdf",width=46.8,height=33.1)
# phangorn::densiTree(poly.trees, consensus = poly.average.tree, scaleX = T, show.tip.label = F, alpha = 0.1, cex=0.7)
# title(xlab = "Proportion of variants different")
# dev.off()
# 
# 
# 
################################
#MINIMUM SPANNING NETWORK

#To reconstruct a MSN we require a genlight object and a distance matrix that represents the genetic distance between samples.
#For the distance, we use the IBS derived distance

#remake the genlight object, but now without the outgroup
#genlight object
gl.poly <- vcfR2genlight(vcf.poly)
ploidy(gl.poly) <- 2
gl.poly@ind.names #check names
library(dartR) #for gl.drop.ind
#gl.poly.wooutgroup<-gl.drop.ind(gl.poly,"5028_radicalis_outgroup") #resulting monomorphic loci are deleted
#metadata_ordered_to_vcf_wo_outgroup<-metadata_ordered_to_vcf[!(metadata_ordered_to_vcf$id %in% c("5028")),]

#The following block of code was eventually not needed.
##make a color palette of all samples, derived from country
#cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$Country_code)))
#colors.df<-as.data.frame(cbind(levels(metadata_ordered_to_vcf$Country_code),cols))
#colnames(colors.df)<-c("Country_code","color")
#library(tidyverse)
##make a left join to integrate the colors in the table, and then only save the color column as a vector (using "pull")
#colors.vector<-metadata_ordered_to_vcf %>% left_join(colors.df, by="Country_code") %>% pull(color)


#now we calculate the minimum spaning network where the nodes are connected by the minimum distance between samples
cols <- distinctColorPalette(length(levels(metadata_ordered_to_vcf$Country_code)))
poly.msn<-poppr.msn(gl.poly,distance,showplot=FALSE,include.ties=TRUE,palette=cols)

#plot the network
set.seed(9) #we set the seed to have the same network each time we plot it
pdf(file="minimum_spanning_network_based_on_IBS_n2_maxalleles2_minmeanDP10_maxmissing0.9.pdf",width=46.8,height=33.1)
plot_poppr_msn(gl.poly,poly.msn,gadj=70)
dev.off()

###################################
#Neighbour joining tree from genlight object using dartR package (!!NOT WORKING YET!!)

#genlight object
gl.poly <- vcfR2genlight(vcf.poly)
ploidy(gl.poly) <- 1
#set populations, in this case the outgroup in a seperate population of the others
pop(gl.poly)<-metadata_ordered_to_vcf$outgroup
#neighbour joining tree, not working for the moment :-(
NJtree<-gl.tree.nj(gl.poly, type="phylogram",outgroup=c("outgroup"))  #outgroup should be a population in genlight object


