#!/bin/bash
# (c) Annelies Haegeman 2015
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to loop over all fastq files in a folder and do some operation on each fastq file

#Specify here the folder where your bam files are:
DIR=/home/genomics/ahaegeman/GBS_Hymenoscyphus/Analyse2_jan2020/GATK/preprocessed_data/Run16

#change directory to specified folder
cd $DIR

# Define the variable FILES that contains all the data sets ending with .fq
FILES=( *.fq )

#Loop over all files and do all the commands
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f _merged_filtered_woREsites.fq`

#do a command, for example diamond blastx or kraken
#diamond blastx -d /home/genomics/bioinf_databases/uniprot/uniprot -q "$SAMPLE"_merged_filtered_woREsites.fq -o "$SAMPLE"_diamond_blastx_uniprot.txt
#kraken2 --db /home/genomics/bioinf_databases/kraken2/NCBI_nt --threads 16 --fastq-input --output "$SAMPLE"_output.txt --report "$SAMPLE"_report.txt "$SAMPLE"_merged_filtered_woREsites.fq

#count how many reads are present for each sample
printf "$SAMPLE" >> numberofreads.txt
printf "\t" >> numberofreads.txt
expr $(cat "$SAMPLE"_merged_filtered_woREsites.fq | wc -l) / 4 >> numberofreads.txt


done