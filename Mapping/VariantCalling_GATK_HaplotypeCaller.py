#!/usr/bin/python3

#===============================================================================
# Description
#===============================================================================

# Elisabeth VEECKMAN november 2016
# modified by Annelies HAEGEMAN december 2019 to work with latest GATK version

#===============================================================================
# Import modules
#===============================================================================

import argparse
import sys
import os
import multiprocessing
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Perform variant calling using the GATK Haplotype Caller.')

# Add positional arguments (mandatory)
parser.add_argument('bam_list',
                    help = 'List of input BAM files, one file per line (parameter -b). Make sure this file has the suffix .list')
parser.add_argument('reference',
                    help = 'Full path and name of the reference genome (parameter -f).')
parser.add_argument('output',
                    help = 'Name of the output VCF file.')

# Add optional arguments
parser.add_argument('-L', '--positions',
                    default = None,
                    type = str,
                    help = 'BED or position list file containing a list of regions or sites where variants should be called (default = None).')
parser.add_argument('-p', '--processes',
                    default = 4,
                    type = int,
                    help = 'Define the number of parallel processes (default 4)')
parser.add_argument('-n', '--ploidy',
                    default = 2,
                    type = str,
                    help = 'Define the ploidy level of the samples (individually) (default 2)')

# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    """
    Print the current date and time to stderr.
    """
    sys.stderr.write('\n----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return

def gvcf_parallel (bam_file, reference = args['reference'], bed = args['positions'],ploidy = args['ploidy']):
    """
    In this function, HaplotypeCaller is used to generate an intermediate genomic gVCF (gVCF) for each bam file.
    This file can then be used for joint genotyping of multiple samples in a very efficient way, 
    which enables rapid incremental processing of samples as they roll off the sequencer, 
    as well as scaling to very large cohort sizes (e.g. the 92K exomes of ExAC).
    """
    basename = os.path.basename(bam_file)
    
    if not os.path.isfile(basename + '.g.vcf.log'):
        if bed == None:
            cmd = 'gatk --java-options "-Xmx8G" HaplotypeCaller --reference ' + reference + ' --input ' + bam_file + '.bam --output ' + basename + '.g.vcf -ERC GVCF -ploidy ' + ploidy + ' 2>>' + basename + '.g.vcf.log'
            print(cmd)
        else:
            cmd = 'gatk --java-options "-Xmx8G" HaplotypeCaller --reference ' + reference + ' --input ' + bam_file + '.bam --intervals ' + bed +  ' --output ' + basename + '.g.vcf -ERC GVCF -ploidy ' + ploidy + ' 2>>' + basename + '.g.vcf.log'
        
        os.system(cmd)
    
    return basename + '.g.vcf'

def combine_gvcf (gvcf_files, reference = args['reference']):
    """
    Combine per-sample gVCF files produced by HaplotypeCaller into a multi-sample gVCF file.
    https://software.broadinstitute.org/gatk/documentation/tooldocs/current/org_broadinstitute_gatk_tools_walkers_variantutils_CombineGVCFs.php
    """
    gvcf_string = ''
    
    for gvcf in gvcf_files:
        gvcf_string = gvcf_string + ' --variant ' + gvcf
    
    if not os.path.isfile('merged.g.vcf'):
        cmd = 'gatk CombineGVCFs --reference ' + reference + gvcf_string + ' --output merged.g.vcf 2>> merged.g.vcf.log'
        os.system(cmd)
    
    return

def genotype_gvcf (reference = args['reference'], bed = args['positions'], output = args['output']):
    """
    Perform joint genotyping on gVCF files produced by HaplotypeCaller.
    https://software.broadinstitute.org/gatk/documentation/tooldocs/current/org_broadinstitute_gatk_tools_walkers_variantutils_GenotypeGVCFs.php
    """

    if bed == None:
        cmd = 'gatk --java-options "-Xmx8G" GenotypeGVCFs --reference ' + reference + ' --variant merged.g.vcf --output ' + output + ' 2>> ' + output + '.log'
    else:
        cmd = 'gatk --java-options "-Xmx8G" GenotypeGVCFs --reference ' + reference + ' --variant merged.g.vcf --intervals ' + bed + ' --output ' + output + ' 2>>' + output + '.log'
    
    os.system(cmd)
    
    return
    
#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    
    print_date()
    
    sys.stderr.write('* Creating gVCF file for each BAM file  ...\n')
     
    with multiprocessing.Pool(args['processes']) as p:
          
         # build a list of tasks (= all names of bam files)
        bam_files = [line.strip()[:-4] for line in open(args['bam_list'], 'r')]
          
         # run tasks in parallel
        gvcf_files = p.map(gvcf_parallel, bam_files)
        
    sys.stderr.write('* Combining gvcf files and writing output to merged.g.vcf ...\n')
    
    combine_gvcf(gvcf_files)
    
    sys.stderr.write('* Genotyping merged gvcf file and writing final vcf to {}...\n'.format(args['output']))
    
    genotype_gvcf()
    
    sys.stderr.write('* Finished\n\n')

    print_date()